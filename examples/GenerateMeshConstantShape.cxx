#include <vtkm/cont/ArrayHandleGroupVec.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/CellSetSingleType.h>

#include <vtkm/exec/CellEdge.h>

#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/ScatterCounting.h>
#include <vtkm/worklet/WorkletMapTopology.h>

#include <vtkm/filter/FilterDataSet.h>

#include <vtkm/cont/testing/MakeTestDataSet.h>
#include <vtkm/cont/testing/Testing.h>

namespace vtkm
{
namespace worklet
{

namespace
{

struct ExtractEdges
{
  ////
  //// BEGIN-EXAMPLE GenerateMeshConstantShapeCount.cxx
  ////
  struct CountEdges : vtkm::worklet::WorkletMapPointToCell
  {
    using ControlSignature = void(CellSetIn cellSet, FieldOut<> numEdges);
    using ExecutionSignature = _2(CellShape, PointCount);
    using InputDomain = _1;

    template<typename CellShapeTag>
    VTKM_EXEC_CONT vtkm::IdComponent operator()(
      CellShapeTag cellShape,
      vtkm::IdComponent numPointsInCell) const
    {
      return vtkm::exec::CellEdgeNumberOfEdges(numPointsInCell, cellShape, *this);
    }
  };
  ////
  //// END-EXAMPLE GenerateMeshConstantShapeCount.cxx
  ////

  ////
  //// BEGIN-EXAMPLE GenerateMeshConstantShapeGenIndices.cxx
  ////
  class EdgeIndices : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    using ControlSignature = void(CellSetIn cellSet, FieldOut<> connectivityOut);
    using ExecutionSignature = void(CellShape, PointIndices, _2, VisitIndex);
    using InputDomain = _1;

    using ScatterType = vtkm::worklet::ScatterCounting;

    template<typename CellShapeTag, typename PointIndexVecType>
    VTKM_EXEC void operator()(CellShapeTag cellShape,
                              const PointIndexVecType& globalPointIndicesForCell,
                              vtkm::Vec<vtkm::Id, 2>& connectivityOut,
                              vtkm::IdComponent edgeIndex) const
    {
      vtkm::IdComponent numPointsInCell =
        globalPointIndicesForCell.GetNumberOfComponents();

      vtkm::IdComponent pointInCellIndex0 = vtkm::exec::CellEdgeLocalIndex(
        numPointsInCell, 0, edgeIndex, cellShape, *this);
      vtkm::IdComponent pointInCellIndex1 = vtkm::exec::CellEdgeLocalIndex(
        numPointsInCell, 1, edgeIndex, cellShape, *this);

      connectivityOut[0] = globalPointIndicesForCell[pointInCellIndex0];
      connectivityOut[1] = globalPointIndicesForCell[pointInCellIndex1];
    }
  };
  ////
  //// END-EXAMPLE GenerateMeshConstantShapeGenIndices.cxx
  ////

  ////
  //// BEGIN-EXAMPLE GenerateMeshConstantShapeInvoke.cxx
  ////
  template<typename CellSetType, typename Device>
  VTKM_CONT vtkm::cont::CellSetSingleType<> Run(const CellSetType& inCellSet, Device)
  {
    VTKM_IS_DYNAMIC_OR_STATIC_CELL_SET(CellSetType);

    vtkm::cont::ArrayHandle<vtkm::IdComponent> edgeCounts;
    vtkm::worklet::DispatcherMapTopology<CountEdges> countEdgeDispatcher;
    countEdgeDispatcher.Invoke(inCellSet, edgeCounts);

    vtkm::worklet::ScatterCounting scatter(edgeCounts, Device());
    this->OutputToInputCellMap =
      scatter.GetOutputToInputMap(inCellSet.GetNumberOfCells());

    vtkm::cont::ArrayHandle<vtkm::Id> connectivityArray;
    vtkm::worklet::DispatcherMapTopology<EdgeIndices> edgeIndicesDispatcher(scatter);
    edgeIndicesDispatcher.Invoke(
      inCellSet, vtkm::cont::make_ArrayHandleGroupVec<2>(connectivityArray));

    vtkm::cont::CellSetSingleType<> outCellSet(inCellSet.GetName());
    outCellSet.Fill(
      inCellSet.GetNumberOfPoints(), vtkm::CELL_SHAPE_LINE, 2, connectivityArray);

    return outCellSet;
  }
  ////
  //// END-EXAMPLE GenerateMeshConstantShapeInvoke.cxx
  ////

  ////
  //// BEGIN-EXAMPLE GenerateMeshConstantShapeMapCellField.cxx
  ////
  template<typename ValueType, typename Storage, typename Device>
  VTKM_CONT vtkm::cont::ArrayHandle<ValueType> ProcessCellField(
    const vtkm::cont::ArrayHandle<ValueType, Storage>& inCellField,
    Device) const
  {
    vtkm::cont::ArrayHandle<ValueType> outCellField;
    vtkm::cont::DeviceAdapterAlgorithm<Device>::Copy(
      vtkm::cont::make_ArrayHandlePermutation(this->OutputToInputCellMap,
                                              inCellField),
      outCellField);
    return outCellField;
  }
  ////
  //// END-EXAMPLE GenerateMeshConstantShapeMapCellField.cxx
  ////

private:
  vtkm::worklet::ScatterCounting::OutputToInputMapType OutputToInputCellMap;
};

} // anonymous namespace

} // namespace worklet
} // namespace vtkm

namespace vtkm
{
namespace filter
{

//// PAUSE-EXAMPLE
namespace
{

//// RESUME-EXAMPLE
class ExtractEdges : public vtkm::filter::FilterDataSet<ExtractEdges>
{
public:
  template<typename Policy, typename Device>
  VTKM_CONT vtkm::cont::DataSet DoExecute(const vtkm::cont::DataSet& inData,
                                          vtkm::filter::PolicyBase<Policy> policy,
                                          Device);

  template<typename T, typename StorageType, typename Policy, typename Device>
  VTKM_CONT bool DoMapField(vtkm::cont::DataSet& result,
                            const vtkm::cont::ArrayHandle<T, StorageType>& input,
                            const vtkm::filter::FieldMetadata& fieldMeta,
                            const vtkm::filter::PolicyBase<Policy>& policy,
                            Device);

private:
  vtkm::worklet::ExtractEdges Worklet;
};

//// PAUSE-EXAMPLE
} // anonymous namespace
//// RESUME-EXAMPLE
} // namespace filter
} // namespace vtkm

namespace vtkm
{
namespace filter
{

//// PAUSE-EXAMPLE
namespace
{

//// RESUME-EXAMPLE
template<typename Policy, typename Device>
inline VTKM_CONT vtkm::cont::DataSet ExtractEdges::DoExecute(
  const vtkm::cont::DataSet& inData,
  vtkm::filter::PolicyBase<Policy> policy,
  Device)
{
  VTKM_IS_DEVICE_ADAPTER_TAG(Device);

  const vtkm::cont::DynamicCellSet& inCells =
    inData.GetCellSet(this->GetActiveCellSetIndex());

  vtkm::cont::CellSetSingleType<> outCells =
    this->Worklet.Run(vtkm::filter::ApplyPolicy(inCells, policy), Device());

  vtkm::cont::DataSet outData;

  outData.AddCellSet(outCells);

  for (vtkm::IdComponent coordSystemIndex = 0;
       coordSystemIndex < inData.GetNumberOfCoordinateSystems();
       ++coordSystemIndex)
  {
    outData.AddCoordinateSystem(inData.GetCoordinateSystem(coordSystemIndex));
  }

  return outData;
}

template<typename T, typename StorageType, typename Policy, typename Device>
inline VTKM_CONT bool ExtractEdges::DoMapField(
  vtkm::cont::DataSet& result,
  const vtkm::cont::ArrayHandle<T, StorageType>& input,
  const vtkm::filter::FieldMetadata& fieldMeta,
  const vtkm::filter::PolicyBase<Policy>&,
  Device)
{
  vtkm::cont::Field output;

  if (fieldMeta.IsPointField())
  {
    output = fieldMeta.AsField(input); // pass through
  }
  else if (fieldMeta.IsCellField())
  {
    output = fieldMeta.AsField(this->Worklet.ProcessCellField(input, Device()));
  }
  else
  {
    return false;
  }

  result.AddField(output);

  return true;
}

//// PAUSE-EXAMPLE
} // anonymous namespace

//// RESUME-EXAMPLE
} // namespace filter
} // namespace vtkm

namespace
{

void CheckOutput(const vtkm::cont::CellSetSingleType<>& cellSet)
{
  std::cout << "Num cells: " << cellSet.GetNumberOfCells() << std::endl;
  VTKM_TEST_ASSERT(cellSet.GetNumberOfCells() == 12 + 8 + 6 + 9,
                   "Wrong # of cells.");

  auto connectivity = cellSet.GetConnectivityArray(vtkm::TopologyElementTagPoint(),
                                                   vtkm::TopologyElementTagCell());
  std::cout << "Connectivity:" << std::endl;
  vtkm::cont::printSummary_ArrayHandle(connectivity, std::cout, true);

  auto connectivityPortal = connectivity.GetPortalConstControl();
  VTKM_TEST_ASSERT(connectivityPortal.Get(0) == 0, "Bad edge index");
  VTKM_TEST_ASSERT(connectivityPortal.Get(1) == 1, "Bad edge index");
  VTKM_TEST_ASSERT(connectivityPortal.Get(2) == 1, "Bad edge index");
  VTKM_TEST_ASSERT(connectivityPortal.Get(3) == 5, "Bad edge index");
  VTKM_TEST_ASSERT(connectivityPortal.Get(68) == 9, "Bad edge index");
  VTKM_TEST_ASSERT(connectivityPortal.Get(69) == 10, "Bad edge index");
}

void TryWorklet()
{
  std::cout << std::endl << "Trying calling worklet." << std::endl;
  vtkm::cont::DataSet inDataSet =
    vtkm::cont::testing::MakeTestDataSet().Make3DExplicitDataSet5();
  vtkm::cont::CellSetExplicit<> inCellSet;
  inDataSet.GetCellSet().CopyTo(inCellSet);

  vtkm::worklet::ExtractEdges worklet;
  vtkm::cont::CellSetSingleType<> outCellSet =
    worklet.Run(inCellSet, VTKM_DEFAULT_DEVICE_ADAPTER_TAG());
  CheckOutput(outCellSet);
}

void TryFilter()
{
  std::cout << std::endl << "Trying calling filter." << std::endl;
  vtkm::cont::DataSet inDataSet =
    vtkm::cont::testing::MakeTestDataSet().Make3DExplicitDataSet5();

  vtkm::filter::ExtractEdges filter;

  // NOTE 2018-03-21: I expect this to fail in the short term. Right now no fields
  // are copied from input to output. The default should be changed to copy them
  // all.
  vtkm::cont::DataSet outDataSet = filter.Execute(inDataSet);

  vtkm::cont::CellSetSingleType<> outCellSet;
  outDataSet.GetCellSet().CopyTo(outCellSet);
  CheckOutput(outCellSet);

  vtkm::cont::Field outCellField = outDataSet.GetField("cellvar");
  VTKM_TEST_ASSERT(outCellField.GetAssociation() ==
                     vtkm::cont::Field::Association::CELL_SET,
                   "Cell field not cell field.");
  vtkm::cont::ArrayHandle<vtkm::Float32> outCellData;
  outCellField.GetData().CopyTo(outCellData);
  std::cout << "Cell field:" << std::endl;
  vtkm::cont::printSummary_ArrayHandle(outCellData, std::cout, true);
  VTKM_TEST_ASSERT(outCellData.GetNumberOfValues() == outCellSet.GetNumberOfCells(),
                   "Bad size of field.");

  auto cellFieldPortal = outCellData.GetPortalConstControl();
  VTKM_TEST_ASSERT(test_equal(cellFieldPortal.Get(0), 100.1), "Bad field value.");
  VTKM_TEST_ASSERT(test_equal(cellFieldPortal.Get(1), 100.1), "Bad field value.");
  VTKM_TEST_ASSERT(test_equal(cellFieldPortal.Get(34), 130.5), "Bad field value.");
}

void DoTest()
{
  TryWorklet();
  TryFilter();
}

} // anonymous namespace

int GenerateMeshConstantShape(int, char* [])
{
  return vtkm::cont::testing::Testing::Run(DoTest);
}
