% -*- latex -*-

\chapter{Array Handles}
\label{chap:ArrayHandle}

\index{array handle|(}

An \keyterm{array handle}, implemented with the \vtkmcont{ArrayHandle}
class, manages an array of data that can be accessed or manipulated by VTK-m
algorithms. It is typical to construct an array handle in the control
environment to pass data to an algorithm running in the execution
environment. It is also typical for an algorithm running in the execution
environment to allocate and populate an array handle, which can then be
read back in the control environment. It is also possible for an array
handle to manage data created by one VTK-m algorithm and passed to another,
remaining in the execution environment the whole time and never copied to
the control environment.

\begin{didyouknow}
  The array handle may have up to two copies of the array, one for the
  control environment and one for the execution environment. However,
  depending on the device and how the array is being used, the array handle
  will only have one copy when possible. Copies between the environments
  are implicit and lazy. They are copied only when an operation needs data
  in an environment where the data is not.
\end{didyouknow}

\vtkmcont{ArrayHandle} behaves like a shared smart pointer in that when the
C++ object is copied, each copy holds a reference to the same array. These
copies are reference counted so that when all copies of the
\vtkmcont{ArrayHandle} are destroyed, any allocated memory is released.

An \textidentifier{ArrayHandle} defines the following methods.

\begin{description}
\item[{\classmember[ArrayHandle]{GetNumberOfValues}}]
  Returns the number of entries in the array.
\item[{\classmember[ArrayHandle]{Allocate}}]
  Resizes the array to include the number of entries given.
  Any previously stored data might be discarded.
\item[{\classmember[ArrayHandle]{Shrink}}]
  Resizes the array to the number of entries given.
  Any data stored in the array is preserved.
  The number of entries must be less than those given in the last call to \classmember{Allocate}.
\item[{\classmember[ArrayHandle]{ReleaseResourcesExecution}}]
  If the \textidentifier{ArrayHandle} is holding any data on a device (such as a GPU), that memory is released to be used elsewhere.
  No data is lost from this call.
  Any data on the released resources is copied to the control environment (the local CPU) before the memory is released.
\item[{\classmember[ArrayHandle]{ReleaseResources}}]
  Releases all memory managed by this \textidentifier{ArrayHandle}.
  Any data in this memory is lost.
\item[{\classmember[ArrayHandle]{SyncControlArray}}]
  Makes sure any data in the execution environment is also available in the control environment.
  This method is useful when timing parallel algorithms and you want to include the time to transfer data between parallel devices and their hosts.
\item[{\classmember[ArrayHandle]{GetPortalControl}}]
  Returns an array portal that can be used to access the data in the array handle in the control environment.
  Array portals are described in Section~\ref{sec:ArrayPortals}.
\item[{\classmember[ArrayHandle]{GetPortalConstControl}}]
  Like \classmember{GetPortalControl} but returns a read-only array portal rather than a read/write array portal.
\item[{\classmember[ArrayHandle]{PrepareForInput}}]
  Readies the data as input to a parallel algorithm.
  See Section~\ref{sec:ArrayHandle:InterfaceToExecutionEnvironment} for more details.
\item[{\classmember[ArrayHandle]{PrepareForOutput}}]
  Readies the data as output to a parallel algorithm.
  See Section~\ref{sec:ArrayHandle:InterfaceToExecutionEnvironment} for more details.
\item[{\classmember[ArrayHandle]{PrepareForInPlace}}]
  Readies the data as input and output to a parallel algorithm.
  See Section~\ref{sec:ArrayHandle:InterfaceToExecutionEnvironment} for more details.
\item[{\classmember[ArrayHandle]{GetDeviceAdapterId}}]
  Returns a \vtkmcont{DeviceAdapterId} describing on which device adapter, if any, the array h
  andle's data is available.
  Device adapter ids are described in Section~\ref{sec:DeviceAdapterTraits}.
\item[{\classmember[ArrayHandle]{GetStorage}}]
  Returns the \vtkmcont{Storage} object that manages the data.
  The type of the storage object is defined by the storage tag template parameter of the \textidentifier{ArrayHandle}.
  Storage objects are described in detail in Chapter~\ref{chap:Storage}.
\end{description}


\section{Creating Array Handles}
\label{sec:CreatingArrayHandles}

\vtkmcont{ArrayHandle} is a templated class with two template parameters.
The first template parameter is the only one required and specifies the
base type of the entries in the array. The second template parameter
specifies the storage used when storing data in the control environment.
Storage objects are discussed later in Chapter~\ref{chap:Storage}, and for
now we will use the default value.

\begin{vtkmexample}{Declaration of the \protect\vtkmcont{ArrayHandle} templated class.}
template<
    typename T,
    typename StorageTag = VTKM_DEFAULT_STORAGE_TAG>
class ArrayHandle;
\end{vtkmexample}

There are multiple ways to create and populate an array handle. The default
\vtkmcont{ArrayHandle} constructor will create an empty array with nothing
allocated in either the control or execution environment. This is
convenient for creating arrays used as the output for algorithms.

\vtkmlisting{Creating an \textidentifier{ArrayHandle} for output data.}{CreateArrayHandle.cxx}

Constructing an \textidentifier{ArrayHandle} that points to a provided C array or
\textcode{std::vector} is straightforward with the
\vtkmcont{make\_ArrayHandle} functions. These functions will make an array
handle that points to the array data that you provide.

\vtkmlisting{Creating an \textidentifier{ArrayHandle} that points to a provided C array.}{ArrayHandleFromCArray.cxx}

\vtkmlisting[ex:ArrayHandleFromVector]{Creating an \textidentifier{ArrayHandle} that points to a provided \textcode{std::vector}.}{ArrayHandleFromVector.cxx}

\emph{Be aware} that \vtkmcont{make\_ArrayHandle} makes a shallow pointer
copy. This means that if you change or delete the data provided, the
internal state of \textidentifier{ArrayHandle} becomes invalid and
undefined behavior can ensue. The most common manifestation of this error
happens when a \textcode{std::vector} goes out of scope. This subtle
interaction will cause the \vtkmcont{ArrayHandle} to point to an
unallocated portion of the memory heap. For example, if the code in
Example~\ref{ex:ArrayHandleFromVector} where to be placed within a callable
function or method, it could cause the \vtkmcont{ArrayHandle} to become
invalid.

\begin{commonerrors}
  Because \textidentifier{ArrayHandle} does not manage data provided by
  \textidentifier{make\_ArrayHandle}, you should only use these as
  temporary objects. Example~\ref{ex:ArrayOutOfScope} demonstrates a method
  of copying one of these temporary arrays into safe managed memory, and
  Section~\ref{sec:ArrayHandle:Populate} describes how to put data directly
  into an \textidentifier{ArrayHandle} object.
\end{commonerrors}

\vtkmlisting[ex:ArrayOutOfScope]{Invalidating an \textidentifier{ArrayHandle} by letting the source \textcode{std::vector} leave scope.}{ArrayOutOfScope.cxx}


\section{Array Portals}
\label{sec:ArrayPortals}

\index{array portal|(}
\index{array handle!portal|(}

An array handle defines auxiliary structures called \keyterm{array portals}
that provide direct access into its data. An array portal is a simple
object that is somewhat functionally equivalent to an STL-type iterator, but
with a much simpler interface. Array portals can be read-only (const) or
read-write and they can be accessible from either the control environment
or the execution environment. All these variants have similar interfaces
although some features that are not applicable can be left out.

An array portal object contains each of the following:
\begin{description}
\item[\classmember{ValueType}] The type for each item in the array.
\item[\classmember{GetNumberOfValues}] A method that returns the number of
  entries in the array.
\item[\classmember{Get}] A method that returns the value at a given index.
\item[\classmember{Set}] A method that changes the value at a given
  index. This method does not need to exist for read-only (const) array
  portals.
\end{description}

The following code example defines an array portal for a simple C array of
scalar values. This definition has no practical value (it is covered by the
more general \vtkmcontinternal{ArrayPortalFromIterators}), but demonstrates
the function of each component.

\vtkmlisting{A simple array portal implementation.}{SimpleArrayPortal.cxx}

Although array portals are simple to implement and use, and array portals'
functionality is similar to iterators, there exists a great deal of code
already based on STL iterators and it is often convienient to interface
with an array through an iterator rather than an array portal. The
\vtkmcont{ArrayPortalToIterators} class can be used to convert an array
portal to an STL-compatible iterator. The class is templated on the array
portal type and has a constructor that accepts an instance of the array
portal. It contains the following features.
\begin{description}
\item[{\classmember[ArrayPortalToIterators]{IteratorType}}]
  The type of an STL-compatible random-access iterator that can provide the same access as the array portal.
\item[{\classmember[ArrayPortalToIterators]{GetBegin}}]
  A method that returns an STL-compatible iterator of type \classmember{IteratorType} that points to the beginning of the array.
\item[{\classmember[ArrayPortalToIterators]{GetEnd}}]
  A method that returns an STL-compatible iterator of type \classmember{IteratorType} that points to the end of the array.
\end{description}

\vtkmlisting{Using \textidentifier{ArrayPortalToIterators}.}{ArrayPortalToIterators.cxx}

As a convenience, \vtkmheader{vtkm/cont}{ArrayPortalToIterators.h} also
defines a pair of functions named \vtkmcont*{ArrayPortalToIteratorBegin}
and \vtkmcont*{ArrayPortalToIteratorEnd}
that each take an array portal as an
argument and return a begin and end iterator, respectively.

\vtkmlisting{Using \textidentifier{ArrayPortalToIteratorBegin} and \textidentifier{ArrayPortalToIteratorEnd}.}{ArrayPortalToIteratorBeginEnd.cxx}

\textidentifier{ArrayHandle} contains two internal type definitions for array
portal types that are capable of interfacing with the underlying data in
the control environment. These are \classmember{PortalControl}
and \classmember{PortalConstControl},
which define read-write and read-only (const)
array portals, respectively.

\textidentifier{ArrayHandle} also contains similar type definitions for
array portals in the execution environment. Because these types are
dependent on the device adapter used for execution, these type definitions are
embedded in a templated class named \classmember{ExecutionTypes}.
Within \classmember{ExecutionTypes} are the type definitions
\classmember{Portal} and \classmember{PortalConst} defining the read-write and
read-only (const) array portals, respectively, for the execution
environment for the given device adapter tag.

Because \vtkmcont{ArrayHandle} is control environment object, it provides the methods \classmember{GetPortalControl} and \classmember{GetPortalConstControl} to get the associated array portal objects.
These methods also have the side effect of refreshing the control environment copy of the data as if you called \classmember{SyncControlArray}.
%Be aware that when an \textidentifier{ArrayHandle} is created with a pointer or \textcode{std::vector}, it is put in a read-only mode, and \classmember{GetPortalControl} can fail (although \classmember{GetPortalConstControl} will still work).
Be aware that calling \classmember{GetPortalControl} will invalidate any copy in the execution environment, meaning that any subsequent use will cause the data to be copied back again.

\vtkmlisting{Using portals from an \textidentifier{ArrayHandle}.}{ControlPortals.cxx}

\begin{didyouknow}
  Most operations on arrays in VTK-m should really be done in the execution
  environment. Keep in mind that whenever doing an operation using a
  control array portal, that operation will likely be slow for large
  arrays. However, some operations, like performing file I/O, make sense in
  the control environment.
\end{didyouknow}

\index{array handle!portal|)}
\index{array portal|)}


\section{Allocating and Populating Array Handles}
\label{sec:ArrayHandle:Allocate}
\label{sec:ArrayHandle:Populate}

\index{array handle!allocate}
\index{Allocate}

\vtkmcont{ArrayHandle} is capable of allocating its own memory. The most
straightforward way to allocate memory is to call the \classmember[ArrayHandle]{Allocate}
method. The \classmember{Allocate} method takes a single argument, which is
the number of elements to make the array.

\vtkmlisting{Allocating an \textidentifier{ArrayHandle}.}{ArrayHandleAllocate.cxx}

\begin{commonerrors}
  The ability to allocate memory is a key difference between
  \textidentifier{ArrayHandle} and many other common forms of smart
  pointers. When one \textidentifier{ArrayHandle} allocates new memory, all
  other \textidentifier{ArrayHandle}s pointing to the same managed memory
  get the newly allocated memory. This can be particularly surprising when
  the originally managed memory is empty. For example, older versions of
  \textcode{std::vector} initialized all its values by setting them to the
  same object. When a \textcode{vector} of \textidentifier{ArrayHandle}s
  was created and one entry was allocated, all entries changed to the same
  allocation.
\end{commonerrors}

\index{array handle!populate}
\index{GetPortalControl}

Once an \textidentifier{ArrayHandle} is allocated, it can be populated by
using the portal returned from \classmember[ArrayHandle]{GetPortalControl}, as described in
Section~\ref{sec:ArrayPortals}. This is roughly the method used by the
readers in the I/O package (Chapter~\ref{chap:FileIO}).

\vtkmlisting{Populating a newly allocated \textidentifier{ArrayHandle}.}{ArrayHandlePopulate.cxx}


\section{Deep Array Copies}
\label{sec:DeepArrayCopies}

\index{deep array copy|(}
\index{array handle!deep copy|(}

As stated previously, an \textidentifier{ArrayHandle} object behaves as a smart pointer that copies references to the data without copying the data itself.
This is clearly faster and more memory efficient than making copies of the data itself and usually the behavior desired.
However, it is sometimes the case that you need to make a separate copy of the data.

To simplify copying the data, \VTKm comes with the \vtkmcont{ArrayCopy} convenience function defined in \vtkmheader{vtkm/cont}{ArrayCopy.h}.
\textidentifier{ArrayCopy} takes the array to copy from (the source) as its first argument and the array to copy to (the destination) as its second argument.
The destination array will be properly reallocated to the correct size.

\vtkmlisting[ex:ArrayCopy]{Using \textidentifier{ArrayCopy}.}{ArrayCopy.cxx}

\begin{didyouknow}
  \textidentifier{ArrayCopy} will copy data in parallel.
  If desired, you can specify the device as the third argument to \textidentifier{ArrayCopy} using either a device adapter tag or a runtime device tracker.
  Both the tags and tracker are described in Chapter~\ref{chap:DeviceAdapter}.
\end{didyouknow}

\index{array handle!deep copy|)}
\index{deep array copy|)}


\section{Compute Array Range}
\label{sec:ComputeArrayRange}

\index{range!array|(}
\index{array handle!range|(}
\index{array handle!minimum|(}
\index{array handle!maximum|(}

It is common to need to know the minimum and/or maximum values in an array.
To help find these values, \VTKm provides the \vtkmcont{ArrayRangeCompute} convenience function defined in \vtkmheader{vtkm/cont}{ArrayRangeCompute.h}.
\textidentifier{ArrayRangeCompute} simply takes an \textidentifier{ArrayHandle} on which to find the range of values.

If given an array with \vtkm{Vec} values, \textidentifier{ArrayRangeCompute} computes the range separately for each component of the \textidentifier{Vec}.
The return value for \textidentifier{ArrayRangeCompute} is \vtkmcont{ArrayHandle}\tparams{\vtkm{Range}}.
This returned array will have one value for each component of the input array's type.
So for example if you call \textidentifier{ArrayRangeCompute} on a \vtkmcont{ArrayHandle}\tparams{\vtkm{Id3}}, the returned array of \textidentifier{Range}s will have 3 values in it.
Of course, when \textidentifier{ArrayRangeCompute} is run on an array of scalar types, you get an array with a single value in it.

Each value of \vtkm{Range} holds the minimum and maximum value for that component.
The \textidentifier{Range} object is documented in Section~\ref{sec:Range}.

\vtkmlisting[ex:ArrayRangeCompute]{Using \textidentifier{ArrayRangeCompute}.}{ArrayRangeCompute.cxx}

\begin{didyouknow}
  \textidentifier{ArrayRangeCompute} will compute the minimum and maximum values in parallel.
  If desired, you can specify the parallel hardware device used for the computation as an optional second argument to \textidentifier{ArrayRangeCompute}.
  You can specify the device using a runtime device tracker, which is documented in Section~\ref{sec:RuntimeDeviceTracker}.
\end{didyouknow}

\index{array handle!maximum|)}
\index{array handle!minimum|)}
\index{array handle!range|)}
\index{range!array|)}


\section{Interface to Execution Environment}
\label{sec:ArrayHandle:InterfaceToExecutionEnvironment}

\index{array handle!execution environment|(}

One of the main functions of the array handle is to allow an array to be
defined in the control environment and then be used in the execution
environment. When using an \textidentifier{ArrayHandle} with filters or
worklets, this transition is handled automatically. However, it is also
possible to invoke the transfer for use in your own scheduled algorithms.

The \textidentifier{ArrayHandle} class manages the transition from control
to execution with a set of three methods that allocate, transfer, and ready
the data in one operation. These methods all start with the prefix
\textmethod{Prepare} and are meant to be called before some operation happens
in the execution environment. The methods are as follows.

\begin{description}
\item[{\classmember[ArrayHandle]{PrepareForInput}}]
  Copies data from the control to the execution environment, if necessary, and readies the data for read-only access.
\item[{\classmember[ArrayHandle]{PrepareForInPlace}}]
  Copies the data from the control to the execution environment, if necessary, and readies the data for both reading and writing.
\item[{\classmember[ArrayHandle]{PrepareForOutput}}]
  Allocates space (the size of which is given as a parameter) in the execution environment, if necessary, and readies the space for writing.
\end{description}

The \classmember{PrepareForInput} and \classmember{PrepareForInPlace} methods
each take a single argument that is the device adapter tag where execution
will take place (see Section~\ref{sec:DeviceAdapterTag} for more
information on device adapter tags). \classmember{PrepareForOutput} takes two
arguments: the size of the space to allocate and the device adapter tag.
Each of these methods returns an array portal that can be used in the
execution environment. \classmember{PrepareForInput} returns an object of type
\classmember[ArrayHandle]{ExecutionTypes}\tparams{\it{}DeviceAdapterTag}\textcode{:\colonhyp}\classmember{PortalConst}
whereas \textcode{PrepareForInPlace} and \textcode{PrepareForOutput} each
return an object of type
\classmember[ArrayHandle]{ExecutionTypes}\tparams{\it{}DeviceAdapterTag}\textcode{:\colonhyp}\classmember{Portal}.

Although these \textmethod{Prepare} methods are called in the control
environment, the returned array portal can only be used in the execution
environment. Thus, the portal must be passed to an invocation of the
execution environment. Typically this is done with a call to
\vtkmcont[DeviceAdapterAlgorithm]{Schedule}. This and other
device adapter algorithms are described in detail in
Section~\ref{sec:DeviceAdapterAlgorithms}, but here is a quick example of
using these execution array portals in a simple functor.

\vtkmlisting{Using an execution array portal from an \textidentifier{ArrayHandle}.}{ExecutionPortals.cxx}

It should be noted that the array handle will expect any use of the
execution array portal to finish before the next call to any
\textidentifier{ArrayHandle} method. Since these \textmethod{Prepare} methods
are typically used in the process of scheduling an algorithm in the
execution environment, this is seldom an issue.

\begin{commonerrors}
  There are many operations on \textidentifier{ArrayHandle} that can
  invalidate the array portals, so do not keep them around indefinitely. It
  is generally better to keep a reference to the
  \textidentifier{ArrayHandle} and use one of the \textmethod{Prepare} each
  time the data are accessed in the execution environment.
\end{commonerrors}

\index{array handle!execution environment|)}

\index{array handle|)}
